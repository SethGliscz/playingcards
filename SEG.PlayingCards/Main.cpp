
// Playing Cards
// Seth Glisczinski

#include <iostream>
#include <conio.h>

using namespace std;

enum Rank
{ Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };

enum Suit
{ Spades, Hearts, Clubs, Diamonds};

struct Card
{
	Rank Rank;
	Suit Suit;
};

int main()
{


	(void)_getch();
	return 0;
}
